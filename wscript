#!/usr/bin/env python
import os

from waflib.extras import autowaf as autowaf
import waflib.Options as Options
import waflib.Scripting as Scripting

# Version of this package (even if built as a child)
LV2CORE_VERSION = '6.0'

# Variables for 'waf dist'
APPNAME = 'lv2core'
VERSION = LV2CORE_VERSION

# Mandatory variables
top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')
    autowaf.set_options(opt)
    opt.add_option('--bundle-only', action='store_true', default=False,
                   dest='bundle_only',
                   help="Only install bundle (not header or pkg-config file)")
    opt.add_option('--copy-headers', action='store_true', default=False,
                   dest='copy_headers',
                   help='Copy headers instead of linking to bundle')

def configure(conf):
    if not hasattr(os.path, 'relpath') and not Options.options.copy_headers:
        conf.fatal(
            'os.path.relpath missing, get Python 2.6 or use --copy-headers')

    conf.load('compiler_c')
    autowaf.configure(conf)

    autowaf.display_msg(conf, "LV2 bundle directory", conf.env['LV2DIR'])
    print('')

def write_news(doap_file):
    import rdflib
    import textwrap
    from time import strftime, strptime

    doap = rdflib.Namespace('http://usefulinc.com/ns/doap#')
    dcs  = rdflib.Namespace('http://ontologi.es/doap-changeset#')
    rdfs = rdflib.Namespace('http://www.w3.org/2000/01/rdf-schema#')
    foaf = rdflib.Namespace('http://xmlns.com/foaf/0.1/')
    rdf  = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')

    m = rdflib.ConjunctiveGraph()

    try:
        m.parse(doap_file, format='n3')
    except:
        print('warning: no DOAP file found, unable to generate NEWS')
        return

    spec = m.value(None, rdf.type, doap.Project)

    entries = {}
    for r in m.triples([spec, doap.release, None]):
        release   = r[2]
        revision  = m.value(release, doap.revision, None) or '9999'
        date      = m.value(release, doap.created, None) or '9999-01-01'
        blamee    = m.value(release, dcs.blame, None)
        changeset = m.value(release, dcs.changeset, None)

        entry = '%s (%s) stable;\n' % (APPNAME, revision)

        if changeset:
            for i in m.triples([changeset, dcs.item, None]):
                entry += '\n  * ' + '\n    '.join(
                    textwrap.wrap(m.value(i[2], rdfs.label, None), width=79))

        entry += '\n\n -- %s <%s>  %s\n\n' % (
            m.value(blamee, foaf.name, None),
            m.value(blamee, foaf.mbox, None).replace('mailto:', ''),
            strftime('%a, %d %b %Y %H:%M:%S +0000', strptime(date, '%Y-%m-%d')))

        entries[revision] = entry

    news = open('NEWS', 'w')
    for e in sorted(entries.keys(), reverse=True):
        news.write(entries[e])
    news.close()

def build(bld):
    # Header "library"
    obj = bld(export_includes = ['.'],
              name            = 'liblv2core',
              target          = 'lv2core')

    # Bundle (data)
    bld.install_files('${LV2DIR}/lv2core.lv2', bld.path.ant_glob('*.ttl'))

    if not Options.options.bundle_only:
        # Header
        bld.install_files('${INCLUDEDIR}', 'lv2.h')
        bld.install_files('${LV2DIR}/lv2core.lv2', 'lv2.h')

        # Pkgconfig file
        autowaf.build_pc(bld, 'LV2CORE', LV2CORE_VERSION, '', [])

        # URI-like header include
        include_dir = os.path.join(bld.env['INCLUDEDIR'], 'lv2/lv2plug.in/ns')
        bundle_dir  = os.path.join(bld.env['LV2DIR'], 'lv2core.lv2')
        if bld.env['COPY_HEADERS']:
            bld.install_files(os.path.join(include_dir, 'lv2core'),
                              bld.path.ant_glob('*.h'))
        else:
            bld.symlink_as(os.path.join(include_dir, 'lv2core'),
                           os.path.relpath(bundle_dir, include_dir))

class Dist(Scripting.Dist):
    fun = 'dist'
    cmd = 'dist'

    def archive(self):
        # Write NEWS file
        write_news('lv2core.doap.ttl')

        # Build distribution
        Scripting.Dist.archive(self)

        # Delete generated NEWS file
        os.remove('NEWS')

class DistCheck(Dist, Scripting.DistCheck):
    fun = 'distcheck'
    cmd = 'distcheck'

    def archive(self):
        Dist.archive(self)
